package com.example.rickmortyapp.dagger;


import com.example.rickmortyapp.viewmodel.CharactersListViewModel;
import com.example.rickmortyapp.viewmodel.CharactersSearchListViewModel;
import com.example.rickmortyapp.viewmodel.CharactersViewModel;

import dagger.Subcomponent;

/**
 * A sub component to create ViewModels. It is called by the
 * {@link com.example.rickmortyapp.viewmodel.ViewModelFactory}.
 */
@Subcomponent
public interface ViewModelSubComponent {
    @Subcomponent.Builder
    interface Builder {
        ViewModelSubComponent build();
    }

    CharactersListViewModel characterListViewModel();
    CharactersViewModel characterViewModel();
    CharactersSearchListViewModel characterSearchListViewModel();
}
