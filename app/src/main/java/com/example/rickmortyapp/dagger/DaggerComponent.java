package com.example.rickmortyapp.dagger;

import android.app.Application;

import com.example.rickmortyapp.view.ui.DetailActivity;
import com.example.rickmortyapp.view.ui.ListActivity;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;


@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
})
public interface DaggerComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        DaggerComponent build();
    }

    void inject(ListActivity activity);

    void inject(DetailActivity activity);
}
