package com.example.rickmortyapp.view.ui;

import android.os.Bundle;

import com.example.rickmortyapp.R;
import com.example.rickmortyapp.dagger.DaggerDaggerComponent;
import com.example.rickmortyapp.databinding.DetailBinding;
import com.example.rickmortyapp.service.model.Character;
import com.example.rickmortyapp.utils.FontManager;
import com.example.rickmortyapp.viewmodel.CharactersViewModel;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

public class DetailActivity extends AppCompatActivity {

    public static final String VIEW_NAME_HEADER_TITLE = "name_character";
    public static final String VIEW_NAME_HEADER_IMAGE = "image_character";
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    public static final String EXTRA_CHARACTER_ID = "EXTRA_CHARACTER_ID";
    private String mCharacterId;
    private DetailBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerDaggerComponent.builder().application(getApplication())
                .build().inject(this);

        binding = DataBindingUtil.setContentView(this, R.layout.detail);

        //noinspection ConstantConditions
        mCharacterId = getIntent().getExtras().getString(EXTRA_CHARACTER_ID);

        // Configure ViewModel
        final CharactersViewModel viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(CharactersViewModel.class);

        binding.setCharacterViewModel(viewModel);

        viewModel.setCharacterID(mCharacterId);
        viewModel.getCharacterObservable().observe(this, new Observer<Character>() {
            @Override
            public void onChanged(@androidx.annotation.Nullable Character characters) {
                if (characters != null) {
                    viewModel.setCharacter(characters);
                    Picasso.get().load(characters.getImage())
                            .resize(300, 300)
                            .noFade()
                            .placeholder(R.drawable.placeholder)
                            .into(binding.ivImageItem);



                } else {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .add(ServerErrorDialogFragment.newInstance(), null)
                            .commitAllowingStateLoss();
                }
            }
        });

        binding.tvGendersText.setTypeface(FontManager.getTypeface(this, FontManager.FONTAWESOME));
        binding.tvStatusText.setTypeface(FontManager.getTypeface(this, FontManager.FONTAWESOME));


    }
}
