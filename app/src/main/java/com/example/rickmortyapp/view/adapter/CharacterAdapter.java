package com.example.rickmortyapp.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.rickmortyapp.R;
import com.example.rickmortyapp.databinding.ItemBinding;
import com.example.rickmortyapp.service.model.Character;
import com.example.rickmortyapp.view.ui.ListActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.MainThread;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder> {

    private final ListActivity.OnCharacterClickListener mListener;

    private List<Character> mCharacterList = new ArrayList<>(0);

    public CharacterAdapter(ListActivity.OnCharacterClickListener onCharacterClickListener) {
        mListener = onCharacterClickListener;
    }

    @MainThread
    public void bindData(final List<Character> characters) {
        if (this.mCharacterList == null) {
            this.mCharacterList = characters;
            notifyItemRangeInserted(0, characters.size());
        } else {
            DiffUtil.DiffResult result = DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return mCharacterList.size();
                }

                @Override
                public int getNewListSize() {
                    return characters.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    return CharacterAdapter.this.mCharacterList.get(oldItemPosition).getId() ==
                            characters.get(newItemPosition).getId();
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    return CharacterAdapter.this.mCharacterList.get(oldItemPosition).getName()
                            .equalsIgnoreCase(characters.get(newItemPosition).getName());
                }
            });

            this.mCharacterList = characters;
            result.dispatchUpdatesTo(this);
        }
    }

    public void addData(List<Character> characters) {
        this.mCharacterList.addAll(characters);
        notifyDataSetChanged();

    }

    class CharacterViewHolder extends RecyclerView.ViewHolder {
        ItemBinding binding;

        CharacterViewHolder(ItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    @Override
    public CharacterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item,
                        parent, false);

        binding.setCallback(mListener);
        return new CharacterViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(CharacterViewHolder holder, final int position) {
        Picasso.get().load(mCharacterList.get(position).getImage())
                .placeholder(R.drawable.placeholder)
                .into(holder.binding.ivImageItem);
        holder.binding.setCharacter(mCharacterList.get(position));
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mCharacterList.size();
    }

    public List<Character> getmCharacterList() {
        return mCharacterList;
    }
}
