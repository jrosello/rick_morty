package com.example.rickmortyapp.view.ui;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.rickmortyapp.R;
import com.example.rickmortyapp.dagger.DaggerDaggerComponent;
import com.example.rickmortyapp.databinding.MainActivityBinding;
import com.example.rickmortyapp.service.model.Character;
import com.example.rickmortyapp.view.adapter.CharacterAdapter;
import com.example.rickmortyapp.view.adapter.EndlessRecyclerOnScrollListener;
import com.example.rickmortyapp.viewmodel.CharactersListViewModel;
import com.example.rickmortyapp.viewmodel.CharactersSearchListViewModel;

import java.util.List;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;

import static com.example.rickmortyapp.view.ui.DetailActivity.EXTRA_CHARACTER_ID;
import static com.example.rickmortyapp.view.ui.DetailActivity.VIEW_NAME_HEADER_IMAGE;
import static com.example.rickmortyapp.view.ui.DetailActivity.VIEW_NAME_HEADER_TITLE;


public class ListActivity extends AppCompatActivity {

    private CharacterAdapter mCharacterAdapter;
    private MainActivityBinding binding;
    private SearchView searchView;

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    EndlessRecyclerOnScrollListener onScrollListener;
    CharactersSearchListViewModel viewModelSearch;

    CharactersListViewModel viewModelCharacters;

    //Pagination
    private int page = 1;

    //Help to search
    private boolean isSearchMode = false;
    private String currentSearchName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerDaggerComponent.builder().application(getApplication())
                .build().inject(this);

        binding = DataBindingUtil.setContentView(this, R.layout.main_activity);

        binding.setIsLoading(true);

        configureAdapter();
        configureViewModels();

        //First call
        callCharactersList();
    }

    private void configureAdapter() {
        // configure recycler view
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        binding.recycler.setLayoutManager(gridLayoutManager);
        mCharacterAdapter = new CharacterAdapter(new OnCharacterClickListener() {
            @Override
            public void onCharacterClicked(View v, Character character) {
                navigateToDetailActivity(v, character);
            }
        });

        onScrollListener = new EndlessRecyclerOnScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                page++;
                if (isSearchMode) {
                    callCharacterSearch();
                } else {
                    callCharactersList();
                }
            }
        };

        binding.recycler.setAdapter(mCharacterAdapter);
        binding.recycler.addOnScrollListener(onScrollListener);
    }

    private void configureViewModels() {
        // Configure viewModelCharacters
        viewModelCharacters = ViewModelProviders.of(this, viewModelFactory)
                .get(CharactersListViewModel.class);


        // Configure viewModelSearch
        viewModelSearch = ViewModelProviders.of(this, viewModelFactory)
                .get(CharactersSearchListViewModel.class);


    }

    /**
     * Navigate to DetailActivity with translate animation in image and name
     *
     * @param v         parent view
     * @param character current character
     */
    private void navigateToDetailActivity(View v, Character character) {
        Intent intent = new Intent(this, DetailActivity.class);
//      Pass data object in the bundle and populate details activity.
        intent.putExtra(EXTRA_CHARACTER_ID, character.getId() + "");
        ActivityOptionsCompat activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                new Pair<>(v.findViewById(R.id.iv_image_item), VIEW_NAME_HEADER_IMAGE),
                new Pair<>(v.findViewById(R.id.tv_name_item), VIEW_NAME_HEADER_TITLE));
        startActivity(intent, activityOptions.toBundle());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                currentSearchName = query;
                callCharacterSearch();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                if (!query.isEmpty()) {
                    page = 1;
                    isSearchMode = true;
                    currentSearchName = query;
                    callCharacterSearch();
                } else {
                    isSearchMode = false;
                    callCharactersList();
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // close search view on back button pressed
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

    private void callCharacterSearch() {
        viewModelSearch.setCharacterPagination(String.valueOf(page));
        viewModelSearch.setCharacterName(currentSearchName);

        if (!viewModelSearch.getCharacterSearchListObservable().hasObservers()) {
            viewModelSearch.getCharacterSearchListObservable().observe(this, new Observer<List<Character>>() {
                @Override
                public void onChanged(@androidx.annotation.Nullable List<Character> characters) {
                    if (characters != null) {
                        binding.setIsLoading(false);
                        if (page == 1) {
                            mCharacterAdapter.bindData(characters);
                        } else {
                            mCharacterAdapter.addData(characters);
                        }
                    } else {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .add(ServerErrorDialogFragment.newInstance(), null)
                                .commitAllowingStateLoss();
                    }
                    onScrollListener.setLoading(false);

                }
            });
        }
    }

    private void callCharactersList() {
        viewModelCharacters.setCharacterPagination(String.valueOf(page));

        if (!viewModelCharacters.getCharacterListObservable().hasObservers()) {
            viewModelCharacters.getCharacterListObservable().observe(this, new Observer<List<Character>>() {
                @Override
                public void onChanged(@androidx.annotation.Nullable List<Character> characters) {
                    if (characters != null) {
                        binding.setIsLoading(false);
                        if (page == 1) {
                            mCharacterAdapter.bindData(characters);
                        } else {
                            mCharacterAdapter.addData(characters);
                        }
                    } else {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .add(ServerErrorDialogFragment.newInstance(), null)
                                .commitAllowingStateLoss();
                    }
                    onScrollListener.setLoading(false);
                }

            });
        }
    }

    // --------------------------------------------------------------------------------------------
    // RecyclerView adapter
    // --------------------------------------------------------------------------------------------
    public interface OnCharacterClickListener {
        void onCharacterClicked(View v, Character character);
    }
}
