package com.example.rickmortyapp.viewmodel;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.rickmortyapp.service.model.Character;
import com.example.rickmortyapp.service.repository.NetworkModule;

import java.util.List;

import javax.inject.Inject;

import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import static android.content.ContentValues.TAG;

public class CharactersSearchListViewModel extends AndroidViewModel {
    private final LiveData<List<Character>> characterSearchListObservable;
    private final MutableLiveData<String> characterName;
    private final MutableLiveData<String> pagination;
    public ObservableField<java.lang.Character> character = new ObservableField<>();

    private static final MutableLiveData ABSENT = new MutableLiveData();

    {
        //noinspection unchecked
        ABSENT.setValue(null);
    }

    @Inject
    public CharactersSearchListViewModel(@NonNull NetworkModule networkModule, @NonNull Application application) {
        super(application);

        this.characterName = new MutableLiveData<>();
        this.pagination = new MutableLiveData<>();

        characterSearchListObservable = Transformations.switchMap(characterName, input -> {
            if (input.isEmpty()) {
                Log.i(TAG, "CharactersViewModel characterID is absent!!!");
                return ABSENT;
            }

            Log.i(TAG, "CharactersViewModel characterID is " + characterName.getValue());

            return networkModule.getSearchCharacters(characterName.getValue(), pagination.getValue());
        });
    }

    /**
     * Expose the LiveData Character list query so the UI can observe it.
     */
    public LiveData<List<Character>> getCharacterSearchListObservable() {
        return characterSearchListObservable;
    }

    public void setCharacterName(String characterName) {
        this.characterName.setValue(characterName);
    }

    public void setCharacterPagination(String pagination) {
        this.pagination.setValue(pagination);
    }

}
