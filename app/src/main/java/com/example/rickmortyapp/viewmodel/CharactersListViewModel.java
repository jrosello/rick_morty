package com.example.rickmortyapp.viewmodel;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.rickmortyapp.service.model.Character;
import com.example.rickmortyapp.service.repository.NetworkModule;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import static android.content.ContentValues.TAG;

public class CharactersListViewModel extends AndroidViewModel {
    private LiveData<List<Character>> characterListObservable;
    private final MutableLiveData<String> pagination;
    private static final MutableLiveData ABSENT = new MutableLiveData();

    {
        //noinspection unchecked
        ABSENT.setValue(null);
    }

    @Inject
    public CharactersListViewModel(@NonNull NetworkModule networkModule, @NonNull Application application) {
        super(application);

        this.pagination = new MutableLiveData<>();

        characterListObservable = Transformations.switchMap(pagination, input -> {
            if (input.isEmpty()) {
                Log.i(TAG, "CharactersViewModel pagination is absent!!!");
                return ABSENT;
            }

            Log.i(TAG, "CharactersViewModel pagination is " + pagination.getValue());
            return networkModule.getCharactersList(pagination.getValue());
        });
    }

    /**
     * Expose the LiveData Character list query so the UI can observe it.
     */
    public LiveData<List<Character>> getCharacterListObservable() {
        return characterListObservable;
    }

    public void setCharacterPagination(String pagination) {
        this.pagination.setValue(pagination);
    }
}
