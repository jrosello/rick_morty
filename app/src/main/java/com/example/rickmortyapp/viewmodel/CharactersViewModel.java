package com.example.rickmortyapp.viewmodel;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.rickmortyapp.service.model.Character;
import com.example.rickmortyapp.service.repository.NetworkModule;

import javax.inject.Inject;

import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import static android.content.ContentValues.TAG;

public class CharactersViewModel extends AndroidViewModel {
    private final LiveData<Character> characterObservable;
    private final MutableLiveData<String> characterID;
    public ObservableField<Character> character = new ObservableField<>();

    private static final MutableLiveData ABSENT = new MutableLiveData();

    {
        //noinspection unchecked
        ABSENT.setValue(null);
    }

    @Inject
    public CharactersViewModel(@NonNull NetworkModule networkModule, @NonNull Application application) {
        super(application);

        this.characterID = new MutableLiveData<>();

        characterObservable = Transformations.switchMap(characterID, input -> {
            if (input.isEmpty()) {
                Log.i(TAG, "CharactersViewModel characterID is absent!!!");
                return ABSENT;
            }

            Log.i(TAG, "CharactersViewModel characterID is " + characterID.getValue());

            return networkModule.getCharacter(characterID.getValue());
        });
    }

    /**
     * Expose the LiveData Character query so the UI can observe it.
     */
    public LiveData<Character> getCharacterObservable() {
        return characterObservable;
    }

    public void setCharacterID(String characterID) {
        this.characterID.setValue(characterID);
    }

    public void setCharacter(Character character) {
        this.character.set(character);
    }
}
