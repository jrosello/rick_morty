package com.example.rickmortyapp.service.repository;

import com.example.rickmortyapp.service.model.Character;
import com.example.rickmortyapp.service.model.api.ApiCharacter;
import com.example.rickmortyapp.service.model.api.ApiCharacterList;
import com.example.rickmortyapp.service.model.api.ResultsItem;

import java.util.ArrayList;
import java.util.List;

public class RickMortyManager {

    public static List<Character> parseCharactersList(ApiCharacterList response) {
        List<Character> characterList = new ArrayList<>();
        if (response != null && response.getResults() != null) {
            for (ResultsItem responseResultsItem : response.getResults()) {
                Character character = new Character();
                character.setId(responseResultsItem.getId());
                character.setName(responseResultsItem.getName());
                character.setStatus(responseResultsItem.getStatus());
                character.setSpecies(responseResultsItem.getSpecies());
                character.setType(responseResultsItem.getType());
                character.setGender(responseResultsItem.getGender());
                character.setImage(responseResultsItem.getImage());
                character.setBorn(responseResultsItem.getOrigin().getName());
                character.setCurrentLocation(responseResultsItem.getLocation().getName());

                characterList.add(character);
            }
        }
        return characterList;
    }

    public static Character parseCharacter(ApiCharacter response) {
        Character character = new Character();
        if (response != null) {
            character.setId(response.getId());
            character.setName(response.getName());
            character.setStatus(response.getStatus());
            character.setSpecies(response.getSpecies());
            character.setType(response.getType());
            character.setGender(response.getGender());
            character.setImage(response.getImage());
            character.setBorn(response.getOrigin().getName());
            character.setCurrentLocation(response.getLocation().getName());
        }
        return character;
    }
}
