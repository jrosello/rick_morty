package com.example.rickmortyapp.service.model.api;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiCharacterList {

    @SerializedName("results")
    private List<ResultsItem> results;

    @SerializedName("info")
    private Info info;

    public void setResults(List<ResultsItem> results) {
        this.results = results;
    }

    public List<ResultsItem> getResults() {
        return results;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public Info getInfo() {
        return info;
    }
}