package com.example.rickmortyapp.service.repository;

import com.example.rickmortyapp.service.model.Character;
import com.example.rickmortyapp.service.model.api.ApiCharacter;
import com.example.rickmortyapp.service.model.api.ApiCharacterList;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class NetworkModule {
    private RickMortyService rickMortyService;

    @Inject
    public NetworkModule(RickMortyService rickMortyService) {
        this.rickMortyService = rickMortyService;
    }

    public LiveData<List<Character>> getCharactersList(String page) {
        final MutableLiveData<List<Character>> data = new MutableLiveData<>();

        rickMortyService.getCharactersList(page).enqueue(new Callback<ApiCharacterList>() {
            @Override
            public void onResponse(Call<ApiCharacterList> call, Response<ApiCharacterList> response) {
                data.setValue(RickMortyManager.parseCharactersList(response.body()));
            }

            @Override
            public void onFailure(Call<ApiCharacterList> call, Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }

    public LiveData<Character> getCharacter(String id) {
        final MutableLiveData<Character> data = new MutableLiveData<>();

        rickMortyService.getCharacter(id).enqueue(new Callback<ApiCharacter>() {
            @Override
            public void onResponse(Call<ApiCharacter> call, Response<ApiCharacter> response) {
                data.setValue(RickMortyManager.parseCharacter(response.body()));
            }

            @Override
            public void onFailure(Call<ApiCharacter> call, Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }

    public LiveData<List<Character>> getSearchCharacters(String name, String page) {
        final MutableLiveData<List<Character>> data = new MutableLiveData<>();

        rickMortyService.getSearchCharacters(name, page).enqueue(new Callback<ApiCharacterList>() {
            @Override
            public void onResponse(Call<ApiCharacterList> call, Response<ApiCharacterList> response) {
                data.setValue(RickMortyManager.parseCharactersList(response.body()));
            }

            @Override
            public void onFailure(Call<ApiCharacterList> call, Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }
}
