package com.example.rickmortyapp.service.repository;

import com.example.rickmortyapp.service.model.api.ApiCharacter;
import com.example.rickmortyapp.service.model.api.ApiCharacterList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RickMortyService {

    String HTTP_API_URL = "https://rickandmortyapi.com/api/";

    @GET("character/")
    Call<ApiCharacterList> getCharactersList(@Query("page") String page);

    @GET("character/{id}/")
    Call<ApiCharacter> getCharacter(@Path("id") String user);

    @GET("character/")
    Call<ApiCharacterList> getSearchCharacters(@Query("name") String user,
                                               @Query("page") String page);
}
